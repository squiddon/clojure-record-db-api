# record-db-rest

Simple personal record database.
Adventures in Clojure and ClojureScript.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein figwheel

## License

Copyright © 2016 Nick Read

## Notes

http://www.braveclojure.com/do-things/

http://clojure-doc.org/articles/tutorials/basic_web_development.html
https://devcenter.heroku.com/articles/clojure-web-application
https://mmcgrana.github.io/2010/08/clojure-rest-api.html
https://blog.interlinked.org/programming/clojure_rest.html

https://github.com/omcljs/om/wiki/Intermediate-Tutorial

https://github.com/JulianBirch/cljs-ajax