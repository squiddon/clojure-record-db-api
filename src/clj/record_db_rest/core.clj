(ns record-db-rest.core
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.util.response :refer [file-response]]
            [ring.middleware.json :refer [wrap-json-params]]
            [ring.middleware.json :refer [wrap-json-response]]
            [record-db-rest.controllers :as controllers]
            [record-db-rest.routes :as routes]))

(defn wrap-log-request [handler]
  (fn [req] ; return handler function
    (println req) ; perform logging
    (handler req))) ; pass the request through to the inner handler

(defn index []
  (file-response "public/html/index.html" {:root "resources"}))

(defroutes app-routes
  (GET "/" [] (index))
  (context "/records" []
    (GET "/" [] 
      (controllers/fetch-records))
    (POST "/"
      [record-name artist]
      (controllers/create-record {:name record-name :artist artist}))
    (context "/:id" [id]
      (PUT "/" [record-name artist]
        (controllers/update-record id {:name record-name :artist artist}))
      (GET "/" []
        (controllers/read-record id))
      (DELETE "/" []
        (controllers/delete-record id))))
  (route/resources "/")
  (route/not-found "Route Not Found"))

(def app
  (-> app-routes
    wrap-json-params
    wrap-json-response
    wrap-log-request))