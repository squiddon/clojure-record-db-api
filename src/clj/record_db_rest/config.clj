(ns record-db-rest.config)

(def db-spec {:classname "org.h2.Driver"
              :subprotocol "h2:file"
              :subname "db/record-db-rest"})