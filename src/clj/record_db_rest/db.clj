(ns record-db-rest.db
  (:require [record-db-rest.config :as config]
            [clojure.java.jdbc :as jdbc]))

(defn create-record
  [params]
  (let [results (jdbc/with-db-connection [db-con config/db-spec]
                  (jdbc/insert! config/db-spec :records params))]
  (assert (= (count results) 1))
  (first (vals results))))

(defn read-record
  [record-id]
  (let [results (jdbc/with-db-connection [db-con config/db-spec]
                  (jdbc/query db-con
                    ["select name, artist from records where id = ?" record-id]))]
  (print record-id)
  (assert (= (count results) 1))
  (first results)))

(defn delete-record
  [record-id]
  (let [results (jdbc/with-db-connection [db-con config/db-spec]
                  (jdbc/delete! config/db-spec :records ["id=?" record-id]))]
  (assert (= (count results) 1))
  (first results)))

(defn update-record
  [id params]
  (let [results (jdbc/with-db-connection [db-con config/db-spec]
                  (jdbc/update! config/db-spec :records params ["id=?" id]))]
  (assert (= (count results) 1))
  (first results)))

(defn fetch-records
  []
  (let [results (jdbc/with-db-connection [db-con config/db-spec]
                  (jdbc/query db-con
                    ["select id, name, artist from records"]))]
  results))