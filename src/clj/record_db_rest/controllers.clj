(ns record-db-rest.controllers
  (:require [record-db-rest.db :as db]
            [clojure.string :as str]
            [ring.util.response :refer [response]]))

(defn create-record
  [params]
  (let [id (db/create-record params)]
  (response "Created!")))

(defn read-record
  [id]
  (let [record (db/read-record id)]
  (response record)))

(defn delete-record
  [id]
  (db/delete-record id)
  (response "Deleted!"))

(defn update-record
  [id params]
  (db/update-record id params)
  (response "Updated!"))

(defn fetch-records
  []
  (let [records (db/fetch-records)]
  (response records)))