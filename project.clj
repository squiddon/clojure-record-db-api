(defproject record-db-rest "0.1.0-SNAPSHOT"
  :description "Record DB RESTful API and frontend"
  :url "https://bitbucket.org/squiddon/clojure-record-db-api"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
    [org.clojure/clojurescript "1.7.170"]
    [compojure "1.4.0"]
    [ring/ring-defaults "0.1.5"]
    [ring/ring-json "0.4.0"]
    [org.clojure/java.jdbc "0.4.2"]
    [com.h2database/h2 "1.3.170"]
    [cljs-ajax "0.5.3"]]

  :plugins [[lein-cljsbuild "1.1.2"]
    [lein-figwheel "0.5.0-3"]]

  :figwheel {:ring-handler record-db-rest.core/app} ; switch to figwheel and use ring handler

  :source-paths ["src/clj" "src/cljs"]
  :resource-paths ["resources"]
  :clean-targets ^{:protect false} ["resources/public/js/out"
                                    "resources/public/js/main.js"]

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/clj" "src/cljs"]
                        :figwheel true
                        :compiler {:output-to "resources/public/js/main.js"
                                   :output-dir "resources/public/js/out"
                                   :asset-path "js/out"
                                   :optimizations :none
                                   :source-map-timestamp true}}]}

  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
