;; Set up records table and add a record

(require '[clojure.java.jdbc :as sql])
(sql/with-connection
  {:classname "org.h2.Driver"
   :subprotocol "h2:file"
   :subname "db/record-db-rest"}

  (sql/create-table :records
    [:id "bigint primary key auto_increment"]
    [:name "longvarchar"]
    [:artist "longvarchar"]
    [:label "longvarchar"])

  (sql/insert-records :records
    {:name "Black One" :artist "Sunn O)))" :label "Southern Lord"}))

;; Drop records table

(require '[clojure.java.jdbc :as sql])
(sql/with-connection
  {:classname "org.h2.Driver"
   :subprotocol "h2:file"
   :subname "db/record-db-rest"}

  (sql/drop-table :records))

;; Have a look

(sql/with-connection
  {:classname "org.h2.Driver"
   :subprotocol "h2:file"
   :subname "db/record-db-rest"}
    (sql/with-query-results res
    ["select name, artist from records"]
    (doall res)))